install:	## Run ruby tests
	bundle install

ruby-test:	## Run ruby tests
	bundle exec rspec .

console:	## Open a PRY console
	bundle exec pry -r ./boot.rb
